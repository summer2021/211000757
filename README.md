# OpenYurt: 云原生边缘计算的云端多租探索（211000757）

## 项目成员
- 陈涛 @chentao_211000757
- 高文俊 @gaowenjun_211000757

## 项目背景
OpenYurt以"Extending your native kubernetes to edge"的设计理念，期待在边缘计算场景探索更多的kubernetes云原生应用可能性。
我们希望基于OpenYurt构建多租户的边缘云原生平台，可适用于边缘的Serverless场景，
这是在很多边缘场景下的切实需求，也将极大地提升项目的竞争力。

本项目主要解决从云端视角的Kubernetes集群的多租管理问题，
确保租户看到完整的Kubernetes同时确保资源的隔离与安全性，确保只能看到租户自身的API资源，对其他租户API资源无感知，无侵入。
通过该项目您将学习到Kubernetes的认证鉴权管理机制，多租户隔离技术，
以及Kubernetes admission开发技术。

## 项目目标
本项目预期完成如下目标，最终交付相应设计文档和实施代码：
- Goals：
  - 基于UID的租户鉴权，准入机制设计和实现；
  - 集群内各类API资源的scope划定，以及与租户UID的关联，
如根据namespace划定，或者设计admission插件，将API资源通过annotation与UID强关联等；
  - API资源的隔离机制和实现，包括可见性，CRUD权限等，可参考yurt-hub思路，从云端视角实现类似的API代理组件；
- Non-Goals：
  - 硬多租隔离在OpenYurt上的落地；

## 项目规划和进展
- 07/20：项目介绍
- 07/23：项目初步方案
- ......
